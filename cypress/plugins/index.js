// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)



/**
 * This file is to help when tester needs to execute script which is in typescript and resolve absolute path locally.
 * Only thing needs to do is add line "pluginsFile": "../cypress.preprocessor.js" in cypress.config.json file.
 * Cypress needs to pass the 'typescript' option to enable TypeScript support.
 * Reference link: https://github.com/cypress-io/cypress-browserify-preprocessor
 * Handle the import with absolute path in test cypress script,
 * please refer to https://stackoverflow.com/questions/58592497/how-to-use-import-with-absolute-paths-in-cypress-tests-built-with-parceljs
 */
const preprocessor = require('@cypress/browserify-preprocessor');
const path = require('path');

const browserifyOptions = preprocessor.defaultOptions;

// resolve paths relative to project root

browserifyOptions.browserifyOptions.paths = [
  // the process.cwd() depends on the cypress process being started from
  // the project root. You can also use an absolute path here.
  require('path').resolve(process.cwd()),
];

// regard paths starting with `/` as project-relative paths

browserifyOptions.browserifyOptions.plugin = browserifyOptions.plugin || [];

// compile spec files and pass typescript when they're running
browserifyOptions.typescript = require.resolve('typescript');
const compileFile = preprocessor(preprocessor.defaultOptions);

/**
 * refer following link to modify after:screen plugin
 * https://github.com/cypress-io/cypress/issues/6648
 * https://github.com/cypress-io/cypress/issues/2259
 */
module.exports = (on, config) => {
  on('file:preprocessor', file => {
    return compileFile(file);
  });
};
