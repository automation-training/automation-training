export const WLBtn: string = 'W/L';
export const PanBtn: string = 'Pan';
export const ZoomBtn: string = 'Zoom';

export const leftViewport: string =
  ':nth-child(1) > app-image-display-element > .viewer > .cornerstone-canvas';
export const contextMenu: string = '.mat-menu-item';
export const imageOverlayLabel: string =
  'body > app-root > as-split > as-split-area:nth-child(1) > div > app-image-display-wrapper:nth-child(1) > app-image-overlay > div.top-left';
export const toolBarWLBtn: string =
  '#mat-button-toggle-1-button > .mat-button-toggle-label-content';
export const toolBarZoomBtn: string =
  '#mat-button-toggle-3-button > .mat-button-toggle-label-content';
export const toogleLabel: string =
  '<div class="mat-button-toggle-focus-overlay"></div>';
export const zoomCheckValueOnLeftViewport: string =
  'body > app-root > as-split > as-split-area:nth-child(1) > div > app-image-display-wrapper:nth-child(1) > app-image-overlay > div.top-right';
export const valueWL: string =
  'body > app-root > as-split > as-split-area:nth-child(1) > div > app-image-display-wrapper:nth-child(1) > app-image-overlay > div.bottom-left';
export const checkboxWL: string =
  '#cdk-overlay-0 > div > div > div > div > button:nth-child(1) > mat-icon';
export const checkboxZoom: string =
  '#cdk-overlay-0 > div > div > div > div > button:nth-child(3) > mat-icon';
