import {
  WLBtn,
  PanBtn,
  ZoomBtn,
  toolBarWLBtn,
  leftViewport,
  toolBarZoomBtn,
  imageOverlayLabel,
  toogleLabel,
  zoomCheckValueOnLeftViewport,
  valueWL,
} from 'src/test/element/context-toolbarElement';

/**
 * Automation Test Script for ToolBar
 */

describe('Test Steps: ToolBar', () => {
  it('Launch Image Viewer', () => {
    cy.visit(
      'http://web-viewer-poc-public.s3-website-us-east-1.amazonaws.com/'
    );
    cy.wait(10000);
    cy.get(imageOverlayLabel)
      .invoke('text')
      .should('eq', ' Series Description: CHEST ROUTINE  5.0  B20f\n');
  });

  it(' Check Tool Menu', () => {
    cy.contains(WLBtn).should('to.be.visible');
    cy.contains(PanBtn).should('to.be.visible');
    cy.contains(ZoomBtn).should('to.be.visible');
  });

  it('Click tool bar WL button', () => {
    cy.get(toolBarWLBtn).click().get(toogleLabel)
  });

  it('Adjust contrast & Brightness(left): Shrink Window from both sides in image', () => {
    cy.get(leftViewport).should('be.visible');

    cy.get(leftViewport)
      .should('be.visible')
      .trigger('mousedown', 'center', { which: 1 })
      .trigger('mousemove', 'left', { which: 1 })
      .trigger('mouseup', { which: 1 });
    cy.get(valueWL)
      .invoke('text')
      .should('eq', 'W/L: 0 / 30');
    cy.wait(1000);
  });

  it('Adjust contrast & Brightness(right): Exapnds Window from both sides in image', () => {
    cy.get(leftViewport)
      .should('be.visible')
      .trigger('mousedown', 'center', { which: 1 })
      .trigger('mousemove', 'right', { which: 1 })
      .trigger('mouseup', { which: 1 });
    cy.get(valueWL)
      .invoke('text')
      .should('eq', 'W/L: 972 / 30');
    cy.wait(1000);
  });

  it('Adjust contrast & Brightness(up): Increase Brightness', () => {
    cy.get(leftViewport)
      .should('be.visible')
      .trigger('mousedown', 'center', { which: 1 })
      .trigger('mousemove', 'top', { which: 1 })
      .trigger('mouseup', { which: 1 });
    cy.get(valueWL)
      .invoke('text')
      .should('eq', 'W/L: 972 / -658');
    cy.wait(1000);
  });

  it('Adjust contrast & Brightness(down): Decrease Brightness', () => {
    cy.get(leftViewport)
      .should('be.visible')
      .trigger('mousedown', 'center', { which: 1 })
      .trigger('mousemove', 'bottom', { which: 1 })
      .trigger('mouseup', { which: 1 });
    cy.get(valueWL)
      .invoke('text')
      .should('eq', 'W/L: 972 / 26');
  });

  it('Zoom tool operate successfully: zoom in', () => {
    cy.get(toolBarZoomBtn).click();
    cy.get(leftViewport)
      .trigger('mousedown', 'center', { which: 1 })
      .trigger('mousemove', 'top', { which: 1 })
      .trigger('mouseup', { which: 1 });
    cy.get(zoomCheckValueOnLeftViewport)
      .invoke('text')
      .should('eq', 'Zoom: 0.27');
  });

  it('Zoom tool operate successfully: zoom out', () => {
    cy.get(toolBarZoomBtn).click();
    cy.get(leftViewport)
      .should('be.visible')
      .trigger('mousedown', 'center', { which: 1 })
      .trigger('mousemove', 'bottom', { which: 1 })
      .trigger('mouseup', { which: 1 });
    cy.get(zoomCheckValueOnLeftViewport)
      .invoke('text')
      .should('eq', 'Zoom: 0.67');
  });
});
