import {
  leftViewport,
  imageOverlayLabel,
  contextMenu,
  checkboxWL,
  checkboxZoom,
  WLBtn,
  ZoomBtn,
} from 'src/test/element/context-toolbarElement';

/**
 * Automation Test Script for Context Menu
 */

describe('Test Steps: Context Menu', () => {
  it('Launch Image Viewer', () => {
    cy.visit(
      'http://web-viewer-poc-public.s3-website-us-east-1.amazonaws.com/'
    );
    cy.wait(10000);
    cy.get(leftViewport).should('be.visible');
    cy.contains('Image Viewer').should('be.visible');
  });

  it('Display Context Menu', () => {
    cy.get(leftViewport).rightclick();
    cy.get(imageOverlayLabel)
      .invoke('text')
      .should('eq', ' Series Description: CHEST ROUTINE  5.0  B20f\n');
  });

  it('W/L is selected in context menu. ', () => {
    cy.contains(contextMenu, WLBtn).should('be.visible');
    cy.get(checkboxWL).should('be.visible');
  });

  it('ZOOM is selected in context menu', () => {
    cy.contains(contextMenu, ZoomBtn).click();
    cy.wait(100);
    cy.get(leftViewport).rightclick();
    cy.get(checkboxZoom).should('be.visible');
    cy.wait(10);
  });

  it('Reset Viewport is visible in context menu', () => {
    cy.contains(contextMenu, 'Reset Viewport').should('be.visible');
  });
});
