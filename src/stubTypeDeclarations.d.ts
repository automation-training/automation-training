/**
 * It would be smarter to install the dev dependency "@types/lodash". But we're
 * not in this case to illustrate ambient module declarations.
 */
declare module 'lodash';
