module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    // ./node_modules/eslint/conf/eslint-recommended.js
    'eslint:recommended',
    // ./node_modules/@typescript-eslint/eslint-plugin/dist/configs/eslint-recommended.js
    'plugin:@typescript-eslint/eslint-recommended',
    // ./node_modules/@typescript-eslint/eslint-plugin/dist/configs/recommended.json
    'plugin:@typescript-eslint/recommended',
    // ./node_modules/@typescript-eslint/eslint-plugin/dist/configs/recommended-requiring-type-checking.json
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    // ./node_modules/eslint-plugin-import/config/errors.js
    'plugin:import/errors',
    // ./node_modules/eslint-plugin-import/config/warnings.js
    'plugin:import/warnings',
    // ./node_modules/eslint-plugin-import/config/typescript.js
    'plugin:import/typescript',
    // ./node_modules/eslint-config-prettier/index.js
    'prettier',
    // ./node_modules/eslint-config-prettier/@typescript-eslint.js
    'prettier/@typescript-eslint',
  ],
  overrides: [
    // relax some rules in tests
    {
      files: ['*.test.ts'],
      rules: {
        'max-lines-per-function': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
      },
    },
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    sourceType: 'module',
  },
  plugins: [
    'import',
    'jsdoc',
    '@typescript-eslint',
    '@typescript-eslint/tslint',
  ],
  rules: {
    // use T[] instead of Array<T>
    '@typescript-eslint/array-type': 'error',
    // use <T> instead of as T
    '@typescript-eslint/consistent-type-assertions': [
      'error',
      {
        assertionStyle: 'angle-bracket',
        objectLiteralTypeAssertions: 'allow-as-parameter',
      },
    ],
    // be explicit
    '@typescript-eslint/explicit-member-accessibility': [
      'error',
      {
        accessibility: 'explicit',
      },
    ],
    // semicolons everywhere
    '@typescript-eslint/member-delimiter-style': [
      'error',
      {
        multiline: {
          delimiter: 'semi',
          requireLast: true,
        },
        singleline: {
          delimiter: 'semi',
          requireLast: false,
        },
      },
    ],
    // use 'unknown' or actual type instead
    '@typescript-eslint/no-explicit-any': 'error',
    // avoid old habits
    '@typescript-eslint/no-extraneous-class': 'error',
    // error handling
    '@typescript-eslint/no-floating-promises': 'error',
    // prefer explicit types
    '@typescript-eslint/no-inferrable-types': 'off',
    // permit useful operator
    '@typescript-eslint/no-non-null-assertion': 'off',
    // prefer explicit properties
    '@typescript-eslint/no-parameter-properties': 'error',
    // prefer es6 imports
    '@typescript-eslint/no-require-imports': 'error',
    // avoid mistakes
    '@typescript-eslint/no-unnecessary-qualifier': 'error',
    // redundant with strict compiler
    '@typescript-eslint/no-unused-vars': 'off',
    // easier to read
    '@typescript-eslint/prefer-for-of': 'error',
    // discourage wrappers
    '@typescript-eslint/prefer-function-type': 'error',
    // consistency and clarity
    '@typescript-eslint/prefer-readonly': 'error',
    // consistency and clarity
    '@typescript-eslint/promise-function-async': 'error',
    // enable consistent lambdas and actions
    '@typescript-eslint/require-await': 'off',
    // avoid mistakes
    '@typescript-eslint/restrict-plus-operands': 'error',
    // no truthy / falsy
    '@typescript-eslint/strict-boolean-expressions': 'error',
    // more concise
    '@typescript-eslint/unified-signatures': 'error',
    // braces as needed
    'arrow-body-style': 'error',
    // consistency
    camelcase: 'error',
    // default setting of '20'
    complexity: 'error',
    // consistency and clarity
    curly: 'error',
    // consistency and clarity
    'default-case': 'error',
    // consistency and clarity
    'dot-notation': 'error',
    // safety
    eqeqeq: ['error', 'always'],
    // consistency
    'id-match': 'error',
    // seems buggy
    'import/named': 'off',
    // prefer explicitly named
    'import/no-default-export': 'error',
    // address debt
    'import/no-deprecated': 'error',
    // maintain package.json
    'import/no-extraneous-dependencies': 'error',
    // force acknowledgement of side-effects
    'import/no-unassigned-import': 'error',
    // incompatible with tsconfig:paths, and redundant with ts
    'import/no-unresolved': 'off',
    // no grouping, with alpha
    'import/order': [
      'error',
      { groups: [], alphabetize: { order: 'asc', caseInsensitive: true } },
    ],
    // redundant with ts
    'jsdoc/no-types': 'error',
    // should be one usually
    'max-classes-per-file': 'error',
    // code organization
    'max-lines': ['error', { max: 600, skipComments: true }],
    // code organization
    'max-lines-per-function': ['error', { max: 100, skipComments: true }],
    // avoid mistakes
    'no-bitwise': 'error',
    // compatibility
    'no-caller': 'error',
    // log instead
    'no-console': 'error',
    // clarify imports
    'no-duplicate-imports': 'error',
    // safety
    'no-eval': 'error',
    // avoid mistakes
    'no-invalid-this': 'error',
    // good idea, but misbehaves with class field initializers
    'no-magic-numbers': 'off',
    // use \n instead
    'no-multi-str': 'error',
    // avoid mistakes
    'no-new-func': 'error',
    // avoid mistakes
    'no-new-wrappers': 'error',
    // use unicode instead
    'no-octal-escape': 'error',
    // prevent mistakes
    'no-param-reassign': 'error',
    // use non-relative paths instead
    'no-restricted-imports': [
      'error',
      {
        patterns: ['../*'],
      },
    ],
    // use forof instead
    'no-restricted-syntax': ['error', 'ForInStatement'],
    // avoid mistakes
    'no-return-await': 'error',
    // avoid mistakes
    'no-sequences': 'error',
    // improve clarity and avoid mistakes
    'no-shadow': ['error', { hoist: 'all' }],
    // avoid mistakes
    'no-template-curly-in-string': 'error',
    // improve error handling
    'no-throw-literal': 'error',
    // avoid mistakes
    'no-unused-expressions': 'error',
    // clarity
    'no-void': 'error',
    // consistency and clarity
    'one-var': ['error', 'never'],
    // clarity
    'padding-line-between-statements': [
      'error',
      {
        blankLine: 'always',
        prev: '*',
        next: 'return',
      },
      {
        blankLine: 'always',
        prev: 'function',
        next: 'function',
      },
    ],
    // consistency and clarity
    'prefer-arrow-callback': 'error',
    // clarity and performance
    'prefer-object-spread': 'error',
    // consistency and security
    'prefer-template': 'error',
    // avoid mistakes
    radix: 'error',
    // deprecated
    'require-jsdoc': 'off',
    // consistency and clarity
    'spaced-comment': ['error', 'always'],
    // deprecated
    'valid-jsdoc': 'off',
    // clarity
    yoda: 'error',
    '@typescript-eslint/tslint/config': [
      'error',
      {
        rules: {
          // utf-8 everywhere
          encoding: true,
          // possibly redundant with jsdoc plugin and prettier?
          'jsdoc-format': [true, 'check-multiline-start'],
          // consistency and clarity
          'match-default-export-name': true,
          // avoid mistakes
          'no-dynamic-delete': true,
          // import instead
          'no-reference-import': true,
          // clarity
          'no-unnecessary-callback-wrapper': true,
          // consistency and clarity
          'prefer-method-signature': true,
          // consistency and clarity
          'prefer-while': true,
          // avoid mistakes
          'switch-final-break': true,
          // explicit types as documentation
          typedef: [
            true,
            'call-signature',
            'arrow-call-signature',
            'parameter',
            'arrow-parameter',
            'property-declaration',
            'variable-declaration',
            'member-variable-declaration',
          ],
        },
      },
    ],
  },
};
