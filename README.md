# Automation Training

## Purpose

Automation testing training for Cypress using Mochawesome

## Project Structure

```
└── cypress/
└── mochawesome-report/
└── node_modules/
└── reporter/
└── src/
    ├──test/
        ├──element
        ├──script/
            └──contextmenu/
                    ├──Test.int.ts   
            └──toolbar/
                    ├──Test.int.ts   
└──package.json
└──package-lock.json
└──cypress.config.json
└──cypress.config.src.json   // as if not used now
└──tsconfig.json
```

## How to Run?
1.Open project --> automation-training

2.Then, --> git fetch && git checkout feature/ContextMenuToolBar 
(on your terminal)

3.Run command:

    npm run cypress:run
    
Here, we are using *src & package.json.*
Also when we use run command it will first run scripts then by that it generates .json file. Then, will create directory "reporter" using (*merge:reports & create:html:report*) and generate html reports with the help of (*generate:html:reports*)

#### Output:

HTML reports will be saved in reporter directory.

**NOTE**:

*Make sure all the dependencies and libraries are installed to run cypress on your pc.*
*Before you run script make sure previous reports and json files are removed otherwise will be overwrite*